package com.atlassian.plugin.graphql.api;

import com.atlassian.plugin.hostcontainer.HostContainer;
import com.atlassian.plugin.module.ModuleFactory;
import com.atlassian.plugins.graphql.api.GraphQLProvidersModuleDescriptor;
import com.atlassian.plugins.graphql.api.GraphQLProvidersModuleDescriptorFactory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class GraphQLProvidersModuleDescriptorFactoryTest {
    @Mock
    private HostContainer hostContainer;
    @Mock
    private ModuleFactory moduleFactory;

    @Test
    public void testGetModuleDescriptor() throws Exception {
        final GraphQLProvidersModuleDescriptorFactory factory = new GraphQLProvidersModuleDescriptorFactory(hostContainer);
        when(hostContainer.create(GraphQLProvidersModuleDescriptor.class)).thenReturn(mock(GraphQLProvidersModuleDescriptor.class));
        GraphQLProvidersModuleDescriptor descriptor = (GraphQLProvidersModuleDescriptor) factory.getModuleDescriptor("graphql");
        assertNotNull(descriptor);
    }

    @Test
    public void testGetModuleDescriptor_InvalidType() throws Exception {
        final GraphQLProvidersModuleDescriptorFactory factory = new GraphQLProvidersModuleDescriptorFactory(hostContainer);
        GraphQLProvidersModuleDescriptor descriptor = (GraphQLProvidersModuleDescriptor) factory.getModuleDescriptor("rest");
        assertNull(descriptor);
    }
}
