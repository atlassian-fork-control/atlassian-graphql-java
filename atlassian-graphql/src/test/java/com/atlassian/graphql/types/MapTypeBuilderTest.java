package com.atlassian.graphql.types;

import com.atlassian.graphql.annotations.GraphQLName;
import com.atlassian.graphql.json.types.JsonRootGraphQLTypeBuilder;
import com.atlassian.graphql.spi.GraphQLTypeBuilderContext;
import com.google.common.collect.ImmutableMap;
import graphql.execution.ExecutionContext;
import graphql.schema.DataFetchingEnvironment;
import graphql.schema.DataFetchingEnvironmentImpl;
import graphql.schema.GraphQLFieldDefinition;
import graphql.schema.GraphQLObjectType;
import graphql.schema.GraphQLSchema;
import org.junit.Test;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;


public class MapTypeBuilderTest {

    @Test
    public void testBuildTypeAndFetchData() throws Exception {
        // build the type
        final JsonRootGraphQLTypeBuilder typeBuilder = new JsonRootGraphQLTypeBuilder();
        final GraphQLObjectType type = (GraphQLObjectType) typeBuilder.buildType(
                ObjectWithMapField.class,
                new GraphQLTypeBuilderContext());
        final GraphQLSchema schema = GraphQLSchema.newSchema().query(type).build();
        final GraphQLFieldDefinition field = type.getFieldDefinition("map");

        // create the source
        final Map<KeyObj, ValueObj> map = ImmutableMap.of(new KeyObj("key"), new ValueObj("value"));
        final ObjectWithMapField source = new ObjectWithMapField(map);

        // fetch the field value
        final DataFetchingEnvironment data =
                DataFetchingEnvironmentImpl.newDataFetchingEnvironment()
                .source(source)
                .fieldDefinition(field)
                .fieldType(field.getType())
                .context(mock(ExecutionContext.class))
                .build();
        final List<Map> keyValuePairList = (List <Map>)schema.getCodeRegistry().getDataFetcher(type, field).get(data);

        // assert
        assertEquals(1, keyValuePairList.size());
        final Map keyValuePair = keyValuePairList.get(0);
        assertEquals("key", ((KeyObj) keyValuePair.get("key")).getKey());
        assertEquals("value", ((ValueObj) keyValuePair.get("value")).getValue());
    }

    public static class ObjectWithMapField {
        @GraphQLName
        private Map<KeyObj, ValueObj> map;

        public ObjectWithMapField(final Map<KeyObj, ValueObj> map) {
            this.map = map;
        }
    }

    public static class KeyObj {
        @GraphQLName
        private String key;

        public KeyObj(final String key) {
            this.key = key;
        }

        public String getKey() {
            return key;
        }
    }

    public static class ValueObj {
        @GraphQLName
        private String value;

        public ValueObj(final String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }
    }
}
