package com.atlassian.graphql.types;

import com.atlassian.graphql.GraphQLTestTypeBuilderImpl;
import com.atlassian.graphql.annotations.GraphQLName;
import com.atlassian.graphql.schema.GraphQLTypeSchemaPrinter;
import com.atlassian.graphql.spi.GraphQLExtensions;
import com.atlassian.graphql.spi.GraphQLTypeBuilder;
import com.atlassian.graphql.spi.GraphQLTypeBuilderContext;
import com.atlassian.graphql.test.json.GraphQLJsonSerializer;
import com.atlassian.graphql.test.json.GraphQLJsonSerializerOptions;
import com.fasterxml.jackson.databind.ObjectMapper;
import graphql.schema.GraphQLObjectType;
import graphql.schema.GraphQLSchema;
import org.junit.Test;

import java.lang.reflect.AnnotatedElement;
import java.lang.reflect.Method;
import java.lang.reflect.Type;
import java.util.EnumSet;
import java.util.concurrent.CompletableFuture;
import java.util.function.Function;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

public class CompletableFutureTypeBuilderTest {
    private final GraphQLJsonSerializer serializer = new GraphQLJsonSerializer(
            new ObjectMapper(),
            EnumSet.of(GraphQLJsonSerializerOptions.ADD_ROOT_FIELD));

    @Test
    public void testSerialize() throws Exception {
        final ObjectWithField obj = new ObjectWithField();
        assertEquals(
                "{\n" +
                "  \"field\" : \"value\"\n" +
                "}",
                serializer.serializeUsingGraphQL(new GraphQLTestTypeBuilderImpl(), ObjectWithField.class, obj));
    }

    @Test
    public void testSerializeWithValueTransformer() throws Exception {
        final ObjectWithField obj = new ObjectWithField();

        // GraphQLExtensions.getValueTransformer() will add '_extensions'
        final GraphQLExtensions extensions = new GraphQLExtensions() {
            @Override
            public Function<Object, Object> getValueTransformer(final Type type, final AnnotatedElement element) {
                return obj -> (obj instanceof String) ? (obj + "_extensions") : obj;
            }
        };

        assertEquals(
                "{\n" +
                        "  \"field\" : \"value_extensions\"\n" +
                        "}",
                serializer.serializeUsingGraphQL(new GraphQLTestTypeBuilderImpl(extensions), ObjectWithField.class, obj));
    }

    @Test
    public void testSchema() {
        final RootGraphQLTypeBuilder typeBuilder = new RootGraphQLTypeBuilder();
        final GraphQLObjectType type = (GraphQLObjectType) typeBuilder.buildType(ObjectWithField.class);
        final GraphQLSchema schema = GraphQLSchema.newSchema().query(type).build();
        assertEquals(
                "type ObjectWithField {\n" +
                "  field: String\n" +
                "}",
                new GraphQLTypeSchemaPrinter(GraphQLTypeSchemaPrinter.Style.Flat).print(schema.getQueryType()));
    }

    @Test
    public void testBuildType() throws NoSuchMethodException {
        GraphQLTypeBuilderContext ctx = mock(GraphQLTypeBuilderContext.class);
        Method getField = ObjectWithField.class.getMethod("getField");
        new CompletableFutureTypeBuilder(mock(GraphQLTypeBuilder.class))
                .buildType("foo", getField.getGenericReturnType(), getField, ctx);

        verify(ctx, times(1)).updateFieldType(eq(String.class), any());
    }

    public static class ObjectWithField {
        @GraphQLName
        public CompletableFuture<String> getField() {
            return CompletableFuture.completedFuture("value");
        }
    }
}
