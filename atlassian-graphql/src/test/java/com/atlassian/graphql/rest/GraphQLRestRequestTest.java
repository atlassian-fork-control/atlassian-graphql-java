package com.atlassian.graphql.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.ImmutableMap;
import org.junit.Test;

import java.io.IOException;
import java.io.StringWriter;

import static org.junit.Assert.assertEquals;

public class GraphQLRestRequestTest {
    private final ObjectMapper mapper = new ObjectMapper();

    @Test
    public void testDeserialize() throws IOException {
        final String str = "{'query':'{ field }','variables':{'argument':'value'},'operationName':'operationName'}".replace("'", "\"");
        final GraphQLRestRequest request = mapper.reader(GraphQLRestRequest.class).readValue(str);
        assertEquals("query", "{ field }", request.getQuery());
        assertEquals("variables", "value", request.getVariables().get("argument"));
        assertEquals("operationName", "operationName", request.getOperationName());
    }

    @Test
    public void testDeserializeId() throws IOException {
        final String str = "{'id':'sha256:7629b549689172a3e2ce000374d868c996c4d46f791032a136c05bff435c9769','variables':{'argument':'value'},'operationName':'operationName'}".replace("'", "\"");
        final GraphQLRestRequest request = mapper.reader(GraphQLRestRequest.class).readValue(str);
        assertEquals("id", "sha256:7629b549689172a3e2ce000374d868c996c4d46f791032a136c05bff435c9769", request.getId());
        assertEquals("variables", "value", request.getVariables().get("argument"));
        assertEquals("operationName", "operationName", request.getOperationName());
    }

    @Test
    public void testSerialize() throws IOException {
        final GraphQLRestRequest request = new GraphQLRestRequest("{ field }", ImmutableMap.of("argument", "value"), "operationName");
        final StringWriter str = new StringWriter();
        mapper.writer().writeValue(str, request);
        assertEquals("{'query':'{ field }','variables':{'argument':'value'},'operationName':'operationName'}".replace("'", "\""), str.toString());
    }
}
