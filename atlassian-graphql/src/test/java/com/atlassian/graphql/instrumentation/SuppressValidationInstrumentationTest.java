package com.atlassian.graphql.instrumentation;

import com.atlassian.graphql.utils.AsyncExecutionStrategyIgnoreUndefinedFields;
import com.atlassian.graphql.utils.GraphQLUtils;
import com.google.common.collect.Sets;
import graphql.ExecutionInput;
import graphql.ExecutionResult;
import graphql.GraphQL;
import graphql.Scalars;
import graphql.execution.ExecutionPath;
import graphql.parser.Parser;
import graphql.schema.GraphQLFieldDefinition;
import graphql.schema.GraphQLObjectType;
import graphql.schema.GraphQLSchema;
import graphql.validation.ValidationError;
import graphql.validation.ValidationErrorType;
import org.junit.Test;

import java.util.Map;

import static org.junit.Assert.assertEquals;

public class SuppressValidationInstrumentationTest {
    @Test
    public void testGraphQLSuppressValidation() {
        final GraphQLSchema schema = GraphQLSchema.newSchema().query(
                GraphQLObjectType.newObject()
                        .name("type")
                        .field(GraphQLFieldDefinition.newFieldDefinition()
                                .name("field")
                                .type(Scalars.GraphQLString)
                                .dataFetcher(env -> "value")))
                .build();

        final SuppressValidationInstrumentation suppressValidation = new SuppressValidationInstrumentation(Sets.newHashSet(ValidationErrorType.FieldUndefined));
        final GraphQL graphql =
                GraphQL.newGraphQL(schema)
                .queryExecutionStrategy(new AsyncExecutionStrategyIgnoreUndefinedFields())
                .instrumentation(suppressValidation)
                .build();
        final ExecutionResult result = graphql.execute("{ field notInSchema }");
        assertEquals("value", ((Map) result.getData()).get("field"));
        assertEquals("Error type", ValidationErrorType.FieldUndefined, ((ValidationError) result.getErrors().get(0)).getValidationErrorType());
        assertEquals("Path", ExecutionPath.rootPath().segment("notInSchema").toList(), result.getErrors().get(0).getPath());
    }

    @Test
    public void testExecuteDocumentSuppressValidation() {
        final GraphQLSchema schema = GraphQLSchema.newSchema().query(
                GraphQLObjectType.newObject()
                .name("type")
                .field(GraphQLFieldDefinition.newFieldDefinition()
                       .name("field")
                       .type(Scalars.GraphQLString)
                       .dataFetcher(env -> "value")))
                .build();

        final SuppressValidationInstrumentation suppressValidation = new SuppressValidationInstrumentation(Sets.newHashSet(ValidationErrorType.FieldUndefined));
        final ExecutionResult result = GraphQLUtils.executeDocument(
                schema,
                new Parser().parseDocument("{ field notInSchema }"),
                new ExecutionInput(null, null, null, null, null),
                new AsyncExecutionStrategyIgnoreUndefinedFields(),
                null,
                suppressValidation).join();
        assertEquals("Error type", ValidationErrorType.FieldUndefined, ((ValidationError) result.getErrors().get(0)).getValidationErrorType());
        assertEquals("Path", ExecutionPath.rootPath().segment("notInSchema").toList(), result.getErrors().get(0).getPath());
    }
}
