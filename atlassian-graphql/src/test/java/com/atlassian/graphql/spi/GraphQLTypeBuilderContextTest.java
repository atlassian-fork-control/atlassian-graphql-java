package com.atlassian.graphql.spi;

import org.junit.Test;

import java.lang.reflect.Type;
import java.util.function.Predicate;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class GraphQLTypeBuilderContextTest {
    @Test
    public void testIsCurrentType() {
        final GraphQLTypeBuilderContext context = new GraphQLTypeBuilderContext();

        context.enterField("field", String.class);
        assertTrue(context.isCurrentType(String.class));

        context.enterField("field2", Long.class);
        assertTrue(context.isCurrentType(Long.class));

        context.exitField();
        assertFalse(context.isCurrentType(Long.class));
        assertTrue(context.isCurrentType(String.class));

        context.exitField();
        assertFalse(context.isCurrentType(Long.class));
        assertFalse(context.isCurrentType(String.class));
    }


    @Test
    public void testIsCurrentTypeWithPredicateMatching() {
        final GraphQLTypeBuilderContext context = new GraphQLTypeBuilderContext();

        @SuppressWarnings("unchecked") Predicate<Type> predicate = mock(Predicate.class);
        when(predicate.test(any())).thenReturn(true);

        context.enterField("field", String.class);
        assertTrue(context.isCurrentType(predicate));

        verify(predicate).test(String.class);
    }

    @Test
    public void testIsCurrentTypeWithPredicateNotMatching() {
        final GraphQLTypeBuilderContext context = new GraphQLTypeBuilderContext();

        @SuppressWarnings("unchecked") Predicate<Type> predicate = mock(Predicate.class);
        when(predicate.test(any())).thenReturn(false);

        context.enterField("field", String.class);
        assertFalse(context.isCurrentType(predicate));

        verify(predicate).test(String.class);
    }
}
