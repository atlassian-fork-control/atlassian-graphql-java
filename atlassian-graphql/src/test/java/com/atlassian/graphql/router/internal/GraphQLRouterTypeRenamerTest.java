package com.atlassian.graphql.router.internal;

import com.atlassian.graphql.router.GraphQLRouteTypeNamePrefixStrategy;
import com.atlassian.graphql.utils.GraphQLQueryPrinter;
import com.google.common.base.Charsets;
import com.google.common.io.CharStreams;
import graphql.language.Document;
import graphql.parser.Parser;
import org.codehaus.jackson.map.ObjectMapper;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.Map;

import static org.junit.Assert.assertEquals;

public class GraphQLRouterTypeRenamerTest {
    private ObjectMapper objectMapper;
    private GraphQLRouterTypeRenamer schemaRenamer;

    @Before
    public void setUp() {
        objectMapper = new ObjectMapper();
        schemaRenamer = new GraphQLRouterTypeRenamer();
    }

    @Test
    public void testRenameQueryTypes() {
        assertRenamedQueryTypes("{\n    field1\n    field2\n}", "{\n    field1\n    field2\n}");
        assertRenamedQueryTypes(
                "fragment user on ConfluenceUser {\n" +
                "    username\n" +
                "}",
                "fragment user on User {\n" +
                "    username\n" +
                "}\n");
    }

    @Test
    public void testRenameSchemaResultTypes() throws Exception {
        final Map schema = loadResourceAsMap("personInterfaceSchema.txt");
        schemaRenamer.renameDataResultTypeNames(new GraphQLRouteTypeNamePrefixStrategy("Confluence"), schema);
        assertEquals(loadResource("expectedRenamedPersonInterfaceSchema.txt").trim(), prettyPrint(schema));
    }

    @Test
    public void testNoRenameForNullStrategy() throws Exception {
        final Map schema = loadResourceAsMap("personInterfaceSchema.txt");
        final String before = prettyPrint(schema);
        schemaRenamer.renameDataResultTypeNames(null, schema);

        final String after = prettyPrint(schema);
        assertEquals("No-op with strategy == null", before, after);
    }

    @Test
    public void testNoRenameForNullData() throws Exception {
        // expect no exception
        schemaRenamer.renameDataResultTypeNames(new GraphQLRouteTypeNamePrefixStrategy("Confluence"), null);
    }

    private void assertRenamedQueryTypes(final String inputQuery, final String renamedQuery) {
        assertEquals(renamedQuery.trim(), renameQueryTypes(inputQuery).trim());
    }

    private String renameQueryTypes(final String query) {
        final GraphQLRouteTypeNamePrefixStrategy strategy = new GraphQLRouteTypeNamePrefixStrategy("Confluence");
        final Document queryDocument = new Parser().parseDocument(query);
        Document newDocument = schemaRenamer.renameQueryTypes(strategy, queryDocument);
        return new GraphQLQueryPrinter().print(newDocument);
    }

    private String prettyPrint(final Map map) throws IOException {
        final StringWriter renamedSchemaStr = new StringWriter();
        objectMapper.writerWithDefaultPrettyPrinter().writeValue(renamedSchemaStr, map);
        return renamedSchemaStr.toString().trim();
    }

    private Map loadResourceAsMap(final String name) throws IOException {
        final String str = loadResource(name);
        return objectMapper.readValue(new StringReader(str), Map.class);
    }

    private static String loadResource(final String name) throws IOException {
        final String resourceName = "com/atlassian/graphql/router/internal/" + name;
        final InputStream stream = GraphQLRouterSchemaMergerTest.class.getClassLoader().getResourceAsStream(resourceName);
        if (stream == null) {
            throw new IllegalArgumentException("Failed to find test resource '" + name + "'");
        }
        return CharStreams.toString(new InputStreamReader(stream, Charsets.UTF_8));
    }
}
