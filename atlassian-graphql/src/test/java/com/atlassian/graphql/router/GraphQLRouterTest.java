package com.atlassian.graphql.router;

import com.atlassian.graphql.annotations.GraphQLName;
import com.atlassian.graphql.provider.RootProviderGraphQLTypeBuilder;
import com.atlassian.graphql.rest.GraphQLRestRequest;
import com.google.common.base.Charsets;
import com.google.common.io.CharStreams;
import graphql.ExecutionResult;
import graphql.GraphQL;
import graphql.introspection.IntrospectionQuery;
import graphql.schema.GraphQLSchema;
import org.codehaus.jackson.map.ObjectMapper;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.util.Map;

import static org.junit.Assert.assertEquals;

public class GraphQLRouterTest {
    @Test
    public void testRouter() throws Exception {
        final GraphQLSchema rootSchema = createLocalSchema("rootType", RootProviderType.class);
        final GraphQL rootGraphQL = GraphQL.newGraphQL(rootSchema).build();
        final GraphQLSchema schema1 = createLocalSchema("type1", ProviderType1.class);
        final GraphQLSchema schema2 = createLocalSchema("type2", ProviderType2.class);

        final GraphQLRouteConfiguration config =
                GraphQLRouteConfiguration.builder()
                        .addRoute(new LocalGraphQLRoute(GraphQLRouteRules.builder().addFieldName("field1").build(), schema1))
                        .addRoute(new LocalGraphQLRoute(GraphQLRouteRules.builder().addFieldName("field2").build(), schema2))
                        .addRoute(new LocalGraphQLRoute(GraphQLRouteRules.builder().allFields().build(), rootGraphQL))
                        .build();

        final GraphQLRouter router = new GraphQLRouter(config);

        final GraphQLRestRequest request = new GraphQLRestRequest("{ rootField field1 field2 }");
        final ExecutionResult result = router.execute(request, null);
        final Map<String, Object> data = result.getData();
        assertEquals(3, data.size());
        assertEquals("rootField", data.get("rootField"));
        assertEquals("field1", data.get("field1"));
        assertEquals("field2", data.get("field2"));
        assertSchemaQuery(router, "expectedRouterSchema_graphql_version_13.0.txt");
    }

    @Test
    public void testRouterWithAliasedFields() throws Exception {
        final GraphQLSchema rootSchema = createLocalSchema("rootType", RootProviderType.class);
        final GraphQL rootGraphQL = GraphQL.newGraphQL(rootSchema).build();
        final GraphQLSchema schema1 = createLocalSchema("type1", ProviderType1.class);
        final GraphQLSchema schema2 = createLocalSchema("type2", ProviderType2.class);

        final GraphQLRouteConfiguration config =
                GraphQLRouteConfiguration.builder()
                        .addRoute(new LocalGraphQLRoute(GraphQLRouteRules.builder().addFieldName("field1").build(), schema1))
                        .addRoute(new LocalGraphQLRoute(GraphQLRouteRules.builder().addFieldName("field2").build(), schema2))
                        .addRoute(new LocalGraphQLRoute(GraphQLRouteRules.builder().allFields().build(), rootGraphQL))
                        .build();

        final GraphQLRouter router = new GraphQLRouter(config);

        final GraphQLRestRequest request = new GraphQLRestRequest("{ rootField f11: field1 f12: field1 field2 }");
        final ExecutionResult result = router.execute(request, null);
        final Map<String, Object> data = result.getData();
        assertEquals(4, data.size());
        assertEquals("rootField", data.get("rootField"));
        assertEquals("field1", data.get("f11"));
        assertEquals("field1", data.get("f12"));
        assertEquals("field2", data.get("field2"));
        assertSchemaQuery(router, "expectedRouterSchema_graphql_version_13.0.txt");
    }

    @SuppressWarnings("unchecked")
    @Test
    public void testRouterWithRouteField() throws Exception {
        final GraphQLSchema rootSchema = createLocalSchema("RootType", RootProviderType.class);
        final GraphQLSchema schema1 = createLocalSchema("LocalType", ProviderType1.class);
        final GraphQLSchema schema2 = createLocalSchema("AppType", ProviderType2.class);

        final GraphQLRouteConfiguration config =
                GraphQLRouteConfiguration.builder()
                        .addRoute(new LocalGraphQLRoute(GraphQLRouteRules.builder().addFieldName("field1").build(), schema1))
                        .addRoute(new LocalGraphQLRoute(GraphQLRouteRules.builder().typeNamePrefix("Confluence").rootFieldName("confluence").allFields().build(), schema2))
                        .addRoute(new LocalGraphQLRoute(GraphQLRouteRules.builder().allFields().build(), rootSchema))
                        .build();
        final GraphQLRouter router = new GraphQLRouter(config);

        final GraphQLRestRequest request = new GraphQLRestRequest("{ rootField field1 confluence { field2 } }");
        final ExecutionResult result = router.execute(request, null);
        final Map<String, Object> data = result.getData();
        assertEquals(3, data.size());
        assertEquals("rootField", data.get("rootField"));
        assertEquals("field1", data.get("field1"));
        assertEquals("field2", ((Map<String, Object>) data.get("confluence")).get("field2"));
        assertSchemaQuery(router, "expectedRouterWithRouteFieldSchema_graphql_version_13.0.txt");
    }

    private static void assertSchemaQuery(final GraphQLRouter router, final String expectedSchemaResourceName) throws IOException {
        final GraphQLRestRequest request = new GraphQLRestRequest(IntrospectionQuery.INTROSPECTION_QUERY);
        final ExecutionResult result = router.execute(request, null);
        final Map<String, Object> data = result.getData();

        // 'directives' are generated in a random order which messes up the test
        ((Map<String, Object>) data.get("__schema")).remove("directives");

        assertEquals(loadResource(expectedSchemaResourceName), serialize(data));
    }

    private static String serialize(final Map<String, Object> data) throws IOException {
        final ObjectMapper mapper = new ObjectMapper();
        final StringWriter writer = new StringWriter();
        mapper.writerWithDefaultPrettyPrinter().writeValue(writer, data);
        return writer.toString();
    }

    private static GraphQLSchema createLocalSchema(final String rootTypeName, final Class providerType) throws Exception {
        return new RootProviderGraphQLTypeBuilder().buildSchema(
                rootTypeName, providerType.newInstance(), null);
    }

    private static String loadResource(final String name) throws IOException {
        final String resourceName = "com/atlassian/graphql/router/" + name;
        final InputStream stream = GraphQLRouterTest.class.getClassLoader().getResourceAsStream(resourceName);
        if (stream == null) {
            throw new IllegalArgumentException("Failed to find test resource '" + name + "'");
        }
        return CharStreams.toString(new InputStreamReader(stream, Charsets.UTF_8));
    }

    public static class RootProviderType {
        @GraphQLName("rootField")
        public String getRootField() {
            return "rootField";
        }
    }

    public static class ProviderType1 {
        @GraphQLName("field1")
        public String getField1() {
            return "field1";
        }
    }

    public static class ProviderType2 {
        @GraphQLName("field2")
        public String getField2() {
            return "field2";
        }
    }

}
