package com.atlassian.graphql.provider;

import com.atlassian.graphql.GraphQLContext;
import com.atlassian.graphql.annotations.GraphQLName;
import com.atlassian.graphql.annotations.GraphQLOperationType;
import com.atlassian.graphql.annotations.GraphQLProvider;
import com.atlassian.graphql.spi.GraphQLProviders;
import com.atlassian.graphql.spi.GraphQLTypeBuilderContext;
import com.atlassian.graphql.test.json.GraphQLJsonSerializer;
import com.atlassian.graphql.test.json.GraphQLJsonSerializerOptions;
import com.atlassian.graphql.utils.AsyncExecutionStrategyWithExecutionListenerSupport;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Lists;
import graphql.schema.GraphQLObjectType;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.EnumSet;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;

@RunWith(MockitoJUnitRunner.class)
public class RootProviderGraphQLTypeBuilder_ResolveParentFieldArgumentsTest {
    private final GraphQLJsonSerializer serializer = new GraphQLJsonSerializer(
            new ObjectMapper(),
            EnumSet.noneOf(GraphQLJsonSerializerOptions.class));

    @SuppressWarnings("unchecked")
    @Test
    public void testParentFieldArguments() throws Exception {
        final List<GraphQLProviders> providers = Lists.newArrayList();
        providers.add(new GraphQLProviders("content", new ContentProvider()));
        providers.add(new GraphQLProviders("content/likes", new LikesProvider()));

        final GraphQLTypeBuilderContext context = new GraphQLTypeBuilderContext();
        final GraphQLObjectType type = new RootProviderGraphQLTypeBuilder().buildType(
                "query", providers, GraphQLOperationType.QUERY, context, null);

        final String query =
               ("{\n" +
                "  content(contentId: '123') {\n" +
                "    likes {\n" +
                "      contentId\n" +
                "    }\n" +
                "  }\n" +
                "}\n").replace("'", "\"");

        final Map<String, List<Map<String, List<Map<String, Object>>>>> response = (Map) serializer.serializeToObjectUsingGraphQL(
                type,
                context.getTypes(),
                new GraphQLContext(),
                null,
                query,
                new AsyncExecutionStrategyWithExecutionListenerSupport());
        assertEquals(
                "contentId argument on 'likes' field resolved correctly",
                "123",
                response.get("content").get(0).get("likes").get(0).get("contentId"));
    }

    @GraphQLProvider
    public static class ContentProvider {
        @GraphQLName
        public List<Content> content(@GraphQLName("contentId") String contentId) {
            return Lists.newArrayList(new Content(contentId));
        }
    }

    @GraphQLProvider
    public static class LikesProvider {
        @GraphQLName
        public List<Like> likes(@GraphQLName("../contentId") String contentId) {
            return Lists.newArrayList(new Like(contentId));
        }
    }

    public static class Content {
        @GraphQLName
        private String contentId;

        public Content(final String contentId) {
            this.contentId = contentId;
        }
    }

    public static class Like {
        @GraphQLName
        private String contentId;

        public Like(final String contentId) {
            this.contentId = contentId;
        }
    }
}
