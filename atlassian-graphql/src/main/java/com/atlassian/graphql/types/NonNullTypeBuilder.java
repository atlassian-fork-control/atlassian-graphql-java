package com.atlassian.graphql.types;

import com.atlassian.graphql.annotations.GraphQLNonNull;
import com.atlassian.graphql.spi.GraphQLTypeBuilder;
import com.atlassian.graphql.spi.GraphQLTypeBuilderContext;
import com.atlassian.graphql.utils.AnnotationsHelper;
import graphql.schema.GraphQLType;

import java.lang.reflect.AnnotatedElement;
import java.lang.reflect.Type;

import static com.atlassian.graphql.utils.ReflectionUtils.getClazz;

/**
 * A builder for GraphQLNonNull objects when the {@link GraphQLNonNull} annotation is applied.
 */
public class NonNullTypeBuilder extends GraphQLTypeBuilderDelegator {
    private final GraphQLTypeBuilder typeBuilder;

    public NonNullTypeBuilder(final GraphQLTypeBuilder typeBuilder) {
        this.typeBuilder = typeBuilder;
    }

    @Override
    public boolean canBuildType(final Type type, final AnnotatedElement element) {
        if (!typeBuilder.canBuildType(type, element)) {
            return false;
        }
        return AnnotationsHelper.hasAnnotation(getClazz(type), GraphQLNonNull.class)
                || (element != null && AnnotationsHelper.hasAnnotation(element, GraphQLNonNull.class));
    }

    @Override
    public GraphQLType buildType(final String typeName, final Type type, final AnnotatedElement element, final GraphQLTypeBuilderContext context) {
        final GraphQLType typeToWrap = typeBuilder.buildType(typeName, type, element, context);
        return typeToWrap instanceof graphql.schema.GraphQLNonNull
                ? typeToWrap
                : graphql.schema.GraphQLNonNull.nonNull(typeToWrap);
    }

    @Override
    protected GraphQLTypeBuilder getTypeBuilder(final Type type, final AnnotatedElement element) {
        return typeBuilder;
    }
}
