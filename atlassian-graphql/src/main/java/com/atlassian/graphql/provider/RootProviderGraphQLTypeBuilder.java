package com.atlassian.graphql.provider;

import com.atlassian.graphql.annotations.GraphQLName;
import com.atlassian.graphql.annotations.GraphQLOperationType;
import com.atlassian.graphql.provider.internal.ProviderFieldTree;
import com.atlassian.graphql.provider.internal.ProviderFieldTreeBuilder;
import com.atlassian.graphql.provider.internal.extensions.GraphQLExtensionsFactory;
import com.atlassian.graphql.spi.GraphQLExtensions;
import com.atlassian.graphql.spi.GraphQLProviders;
import com.atlassian.graphql.spi.GraphQLTypeBuilder;
import com.atlassian.graphql.spi.GraphQLTypeBuilderContext;
import com.atlassian.graphql.types.ObjectTypeBuilder;
import com.atlassian.graphql.types.RootGraphQLTypeBuilder;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import graphql.schema.GraphQLFieldDefinition;
import graphql.schema.GraphQLObjectType;
import graphql.schema.GraphQLSchema;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.function.BiConsumer;
import java.util.function.Function;

import static java.util.Objects.requireNonNull;

/**
 * A builder for GraphQLObjectType objects with query fields for a set of provider object methods.
 * This builder differs from {@link RootGraphQLTypeBuilder} in the following ways:
 * - Only method provided fields are supported
 * - @GraphQLName on a method supports a path, such as 'content/child'
 * - A graph is provided for multiple objects instead of a single class
 */
public class RootProviderGraphQLTypeBuilder {
    private final Function<GraphQLExtensions, GraphQLTypeBuilder> typeBuilderSupplier;
    private final GraphQLExtensions extensions;

    public RootProviderGraphQLTypeBuilder() {
        this(RootGraphQLTypeBuilder::new, null);
    }

    /**
     * Constructor
     * @param extensions Optionally provides extensions to the graph-ql type system
     */
    public RootProviderGraphQLTypeBuilder(
            final Function<GraphQLExtensions, GraphQLTypeBuilder> typeBuilderSupplier,
            final GraphQLExtensions extensions) {

        this.typeBuilderSupplier = requireNonNull(typeBuilderSupplier);
        this.extensions = extensions;
    }

    /**
     * Build a {@link GraphQLSchema} with query fields for a set of methods.
     * The query fields are placed at the paths specified by the {@link GraphQLName} annotation,
     * applied to the provider classes and their methods.
     *
     * @param rootTypeName The root type name
     * @param provider The provider object whose methods marked with {@link GraphQLName} will become query fields
     * @param errorHandler An error handler that will be invoked if an error occurs while building a provider query field
     * @return The built graph-ql schema
     */
    public GraphQLSchema buildSchema(
            final String rootTypeName,
            final Object provider,
            final BiConsumer<Object, Exception> errorHandler) {

        return buildSchema(rootTypeName, Lists.newArrayList(new GraphQLProviders("", provider)), errorHandler);
    }

    /**
     * Build a {@link GraphQLSchema} with query fields for a set of methods.
     * The query fields are placed at the paths specified by the {@link GraphQLName} annotation,
     * applied to the provider classes and their methods.
     *
     * @param rootTypeName The root type name
     * @param providers The provider objects whose methods marked with {@link GraphQLName} will become query fields
     * @param errorHandler An error handler that will be invoked if an error occurs while building a provider query field
     * @return The built graph-ql schema
     */
    public GraphQLSchema buildSchema(
            final String rootTypeName,
            final List<GraphQLProviders> providers,
            final BiConsumer<Object, Exception> errorHandler) {

        return buildSchema(rootTypeName, rootTypeName, providers, errorHandler);
    }

    /**
     * Build a {@link GraphQLSchema} with query fields for a set of methods.
     * The query fields are placed at the paths specified by the {@link GraphQLName} annotation,
     * applied to the provider classes and their methods.
     *
     * @param queryTypeName The root query type name
     * @param mutationTypeName The root mutation type name
     * @param providers The provider objects whose methods marked with {@link GraphQLName} will become query fields
     * @param errorHandler An error handler that will be invoked if an error occurs while building a provider query field
     * @return The built graph-ql schema
     */
    public GraphQLSchema buildSchema(
            final String queryTypeName,
            final String mutationTypeName,
            final List<GraphQLProviders> providers,
            final BiConsumer<Object, Exception> errorHandler) {

        requireNonNull(providers);

        final GraphQLTypeBuilderContext context = new GraphQLTypeBuilderContext(extensions);
        final GraphQLObjectType queryType = buildType(
                queryTypeName, providers, GraphQLOperationType.QUERY, context, errorHandler);
        final GraphQLObjectType mutationType = buildType(
                mutationTypeName, providers, GraphQLOperationType.MUTATION, context, errorHandler);
        if (queryType == null) {
            throw new IllegalArgumentException("No GraphQL query provider fields have been provided");
        }
        final GraphQLSchema schema =
                GraphQLSchema.newSchema()
                .query(queryType)
                .mutation(mutationType)
                .build(Sets.newLinkedHashSet(context.getTypes().values()));
        return schema;
    }

    /**
     * Build a {@link GraphQLObjectType} with query fields for a set of provider methods.
     * The query fields are placed at the paths specified by the {@link GraphQLName} annotation,
     * applied to the provider classes and their methods.
     *
     * @param queryTypeName The name of the top-level graph-ql type
     * @param provider The provider object whose methods marked with {@link GraphQLName} will become query fields
     * @param context The {@link GraphQLTypeBuilderContext} that collects the graph-ql types as they're built
     * @return The built graph-ql schema type
     */
    public GraphQLObjectType buildType(
            final String queryTypeName,
            final Object provider,
            final GraphQLTypeBuilderContext context) {

        return buildType(
                queryTypeName,
                Lists.newArrayList(new GraphQLProviders("", provider)),
                GraphQLOperationType.QUERY,
                context,
                null);
    }

    private GraphQLObjectType buildType(
            final String queryTypeName,
            final Object provider,
            final GraphQLExtensions extensions,
            final GraphQLTypeBuilderContext context) {

        return buildType(
                queryTypeName,
                Lists.newArrayList(new GraphQLProviders("", provider)),
                GraphQLOperationType.QUERY,
                extensions,
                context,
                null);
    }

    /**
     * Build a {@link GraphQLObjectType} with query fields for a set of provider methods.
     * The query fields are placed at the paths specified by the {@link GraphQLName} annotation,
     * applied to the provider classes and their methods.
     *
     * @param queryTypeName The name of the top-level graph-ql type
     * @param providers The provider objects whose methods marked with {@link GraphQLName} will become query fields
     * @param context The {@link GraphQLTypeBuilderContext} that collects the graph-ql types as they're built
     * @param errorHandler An error handler that will be invoked if an error occurs while building a provider query field
     * @return The built graph-ql schema type
     */
    public GraphQLObjectType buildType(
            final String queryTypeName,
            final List<GraphQLProviders> providers,
            final GraphQLOperationType operationType,
            final GraphQLTypeBuilderContext context,
            final BiConsumer<Object, Exception> errorHandler) {

        return buildType(queryTypeName, providers, operationType, extensions, context, errorHandler);
    }

    private GraphQLObjectType buildType(
            final String queryTypeName,
            final List<GraphQLProviders> providers,
            final GraphQLOperationType operationType,
            final GraphQLExtensions extensions,
            final GraphQLTypeBuilderContext context,
            final BiConsumer<Object, Exception> errorHandler) {

        requireNonNull(queryTypeName);
        requireNonNull(providers);
        requireNonNull(context);

        final ProviderFieldTree fieldTree =
                new ProviderFieldTreeBuilder()
                .build(providers, queryTypeName, operationType);
        final GraphQLExtensions allExtensions = GraphQLExtensionsFactory.getExtensions(
                this, providers, fieldTree, extensions, errorHandler);
        final GraphQLTypeBuilder typeBuilder = typeBuilderSupplier.apply(allExtensions);

        context.setTypeBuilder(typeBuilder);
        context.setExtensions(allExtensions);
        context.setProviderGraphQLTypeBuilder((typeName, provider) ->
                buildType(typeName, provider, allExtensions, context));
        return buildType(fieldTree, typeBuilder, context, allExtensions, errorHandler);
    }

    private GraphQLObjectType buildType(
            final ProviderFieldTree fieldTree,
            final GraphQLTypeBuilder typeBuilder,
            final GraphQLTypeBuilderContext context,
            final GraphQLExtensions allExtensions,
            final BiConsumer<Object, Exception> errorHandler) {

        final List<GraphQLFieldDefinition> fields = new ArrayList<>();
        for (final ProviderFieldTree field : fieldTree.getFields()) {
            buildFields(fields, field, typeBuilder, context, allExtensions, errorHandler);
        }
        if (fields.isEmpty()) {
            return null;
        }

        final GraphQLObjectType.Builder builder = GraphQLObjectType.newObject().name(fieldTree.getTypeName());
        for (final GraphQLFieldDefinition field : fields) {
            builder.field(field);
        }
        return builder.build();
    }

    public void buildFields(
            final List<GraphQLFieldDefinition> fields,
            final ProviderFieldTree field,
            final GraphQLTypeBuilder typeBuilder,
            final GraphQLTypeBuilderContext context,
            final GraphQLExtensions allExtensions,
            final BiConsumer<Object, Exception> errorHandler) {

        requireNonNull(fields);
        requireNonNull(field);
        requireNonNull(typeBuilder);
        requireNonNull(context);

        GraphQLFieldDefinition fieldDefinition = null;

        // if specified, build the field with the method return type here
        if (field.getProviderMethod() != null) {
            fieldDefinition = buildMethodField(
                    field.getFieldName(),
                    typeBuilder,
                    context,
                    field.getProvider(),
                    field.getProviderMethod(),
                    allExtensions,
                    errorHandler);
        } else {
            // otherwise, build an intermediate branch
            context.enterField(field.getFieldName(), null);
            final GraphQLObjectType fieldType = buildType(field, typeBuilder, context, allExtensions, errorHandler);
            if (fieldType != null) {
                fieldDefinition =
                        GraphQLFieldDefinition.newFieldDefinition()
                        .name(field.getFieldName())
                        .type(fieldType)
                        .dataFetcher(environment -> new Object())
                        .build();
            }
            context.exitField();
        }

        // add any field query arguments
        if (fieldDefinition != null) {
            fields.add(fieldDefinition);
        }
    }

    private GraphQLFieldDefinition buildMethodField(
            final String fieldName,
            final GraphQLTypeBuilder typeBuilder,
            final GraphQLTypeBuilderContext context,
            final Object provider,
            final Method method,
            final GraphQLExtensions allExtensions,
            final BiConsumer<Object, Exception> errorHandler) {

        try {
            return ObjectTypeBuilder.buildField(fieldName, method.getGenericReturnType(), method, provider, typeBuilder, context, allExtensions);
        } catch (final Exception ex) {
            if (errorHandler == null) {
                throw ex;
            }
            errorHandler.accept(provider, ex);
            return null;
        }
    }
}
