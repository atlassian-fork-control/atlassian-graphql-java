package com.atlassian.graphql.json.jersey;

import graphql.GraphQLException;

/**
 * The exception that's thrown when an HTTP response other than 200 (OK) is returned from
 * a Jersey style GraphQL fetcher method.
 */
public class GraphQLRestException extends GraphQLException {
    private final int statusCode;

    public GraphQLRestException(String message, int statusCode) {
        this(message, null, statusCode);
    }

    public GraphQLRestException(String message, Throwable cause, int statusCode) {
        super(message, cause);
        this.statusCode = statusCode;
    }

    public int getStatusCode() {
        return statusCode;
    }
}
