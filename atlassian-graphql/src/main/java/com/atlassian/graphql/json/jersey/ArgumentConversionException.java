package com.atlassian.graphql.json.jersey;

/**
 * Exception thrown when an error occurs converting a GraphQL argument
 * This is part of the API for implementations of {@link ArgumentConverter}
 */
@SuppressWarnings("WeakerAccess")
public final class ArgumentConversionException extends GraphQLRestException {
    private static final int DEFAULT_STATUS_CODE = 400; // bad request

    public ArgumentConversionException(String message) {
        super(message, DEFAULT_STATUS_CODE);
    }

    public ArgumentConversionException(String message, Throwable cause) {
        super(message, cause, DEFAULT_STATUS_CODE);
    }

    public ArgumentConversionException(String message, int statusCode) {
        super(message, statusCode);
    }

    public ArgumentConversionException(String message, Throwable cause, int statusCode) {
        super(message, cause, statusCode);
    }
}
