package com.atlassian.graphql.datafetcher;

import com.atlassian.graphql.GraphQLContext;
import com.atlassian.graphql.GraphQLFetcherException;
import com.atlassian.graphql.annotations.GraphQLName;
import com.atlassian.graphql.annotations.expansions.GraphQLExpansionParam;
import com.atlassian.graphql.spi.GraphQLExtensions;
import com.atlassian.graphql.utils.AnnotationsHelper;
import com.atlassian.graphql.utils.GraphQLExpansionEvaluator;
import com.atlassian.graphql.utils.InputObjectDeserializer;
import com.google.common.base.Defaults;
import com.google.common.collect.Lists;
import graphql.language.Field;
import graphql.language.FragmentDefinition;
import graphql.schema.DataFetcher;
import graphql.schema.DataFetchingEnvironment;
import graphql.schema.GraphQLOutputType;
import graphql.schema.GraphQLType;

import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;

import static com.atlassian.graphql.utils.ReflectionUtils.getClazz;
import static java.util.Objects.requireNonNull;

/**
 * A {@link DataFetcher} that invokes a method.
 */
public class MethodDataFetcher implements DataFetcher {
    private final Method method;
    private final String fieldName;
    private final Object source;
    private final GraphQLOutputType responseType;
    private final List<String> currentFieldPath;
    private final Map<String, GraphQLType> allTypes;
    private final GraphQLExtensions extensions;
    private final InputObjectDeserializer inputObjectDeserializer = new InputObjectDeserializer();

    public MethodDataFetcher(final Method method) {
        this(method, null, null, null, null, null, null);
    }

    public MethodDataFetcher(
            final Method method,
            final String fieldName,
            final Object source,
            final GraphQLOutputType responseType,
            final List<String> currentFieldPath,
            final Map<String, GraphQLType> allTypes,
            final GraphQLExtensions extensions) {

        this.method = requireNonNull(method);
        this.fieldName = fieldName;
        this.source = source;
        this.responseType = responseType;
        this.currentFieldPath = currentFieldPath;
        this.allTypes = allTypes;
        this.extensions = extensions;
    }

    protected String getFieldName() {
        return fieldName;
    }

    protected Object getSource() {
        return source;
    }

    protected Method getMethod() {
        return method;
    }

    protected List<String> getCurrentFieldPath() {
        return currentFieldPath;
    }

    protected GraphQLExtensions getExtensions() {
        return extensions;
    }

    @Override
    public Object get(final DataFetchingEnvironment env) {
        requireNonNull(env);

        // make the arguments to pass to the method
        final Type methodReturnType = method.getGenericReturnType();
        final GraphQLContext context =
                env.getContext() instanceof GraphQLContext
                ? env.getContext()
                : new GraphQLContext();
        final Object[] methodArgs = calculateMethodArgs(
                methodReturnType,
                env,
                env.getArguments(),
                getQueryField(env.getFields(), fieldName),
                context);

        // invoke the method
        Object response;
        try {
            response = method.invoke(getSource(env), methodArgs);
        } catch (ReflectiveOperationException ex) {
            throw new GraphQLFetcherException(
                    env.getExecutionStepInfo() != null ? env.getExecutionStepInfo().getPath() : null,
                    unwrapError(ex));
        }
        return response;
    }

    private Object getSource(final DataFetchingEnvironment env) {
        return this.source != null ? this.source : env.getSource();
    }

    public static Exception unwrapError(final ReflectiveOperationException ex) {
        if (!(ex instanceof InvocationTargetException)) {
            return ex;
        }
        final Throwable target = ((InvocationTargetException) ex).getTargetException();
        if (!(target instanceof Exception)) {
            return ex;
        }
        return (Exception) target;
    }

    private static Field getQueryField(final List<Field> fields, final String fieldName) {
        if (fields == null || fieldName == null) {
            return null;
        }
        return fields.stream().filter(f -> f.getName().equals(fieldName)).findFirst().orElse(null);
    }

    /**
     * Calculate the argument values that will be passed to the method.
     */
    protected Object[] calculateMethodArgs(
            final Type methodReturnType,
            final DataFetchingEnvironment env,
            final Map<String, Object> arguments,
            final Field queryField,
            final GraphQLContext graphqlContext) {

        final Annotation[][] parameterAnnotations = method.getParameterAnnotations();
        final Parameter[] parameters = method.getParameters();

        final List<Object> list = new ArrayList<>();
        for (int i = 0; i < parameters.length; i++) {
            final GraphQLName graphQLName = AnnotationsHelper.getAnnotation(parameterAnnotations[i], GraphQLName.class);
            final GraphQLExpansionParam graphQLExpansionParam = AnnotationsHelper.getAnnotation(parameterAnnotations[i], GraphQLExpansionParam.class);

            list.add(calculateMethodArg(
                    methodReturnType,
                    env,
                    graphqlContext,
                    arguments,
                    queryField,
                    parameters[i],
                    graphQLName,
                    graphQLExpansionParam));
        }
        return list.toArray(new Object[list.size()]);
    }

    protected Object calculateMethodArg(
            final Type methodReturnType,
            final DataFetchingEnvironment env,
            final GraphQLContext graphqlContext,
            final Map<String, Object> arguments,
            final Field queryField,
            final Parameter parameter,
            final GraphQLName graphQLName,
            final GraphQLExpansionParam graphQLExpansionParam) {

        // DataFetchingEnvironment parameter
        if (parameter.getType() == DataFetchingEnvironment.class) {
            return env;
        }

        // @GraphQLName parameter becomes a graph-ql field query argument
        if (graphQLName != null) {
            Object value = convertFieldArgumentValue(
                    env,
                    getArgument(graphQLName.value(), graphqlContext, arguments),
                    parameter.getParameterizedType());
            if (value != null) {
                return value;
            }
        }

        // @GraphQLExpansionParam 'expand' parameter is calculated from the fields specified in the query
        if (graphQLExpansionParam != null) {
            return evaluateExpandArgAsString(env, methodReturnType, queryField);
        }

        // GraphQLContext provides other parameter values (eg. UriInfo)
        if (graphqlContext.hasInjectedParameterValue(parameter.getType())) {
            return graphqlContext.getInjectedParameterValue(parameter.getType());
        }

        // else use a default value from the parameter type
        return getDefaultValue(parameter.getType());
    }

    /**
     * Get a field's argument value from it's name.
     */
    private static Object getArgument(
            final String argumentName,
            final GraphQLContext graphqlContext,
            final Map<String, Object> arguments) {

        return arguments.containsKey(argumentName)
               ? arguments.get(argumentName)
               : getParentArgument(argumentName, graphqlContext);
    }

    /**
     * If the @GraohQLName argument name starts with the special '../' prefix, then look for
     * a parent field's argument value.
     */
    private static Object getParentArgument(
            final String argumentName,
            final GraphQLContext graphqlContext) {

        return getParentArgument(argumentName, 0, graphqlContext, 0);
    }

    private static Object getParentArgument(
            final String argumentName,
            final int i,
            final GraphQLContext graphqlContext,
            final int stepsBack) {

        if (argumentName.startsWith("../", i)) {
            return getParentArgument(argumentName, i + 3, graphqlContext, stepsBack + 1);
        }
        if (stepsBack == 0) {
            return null;
        }
        return graphqlContext.getParentArgumentValue(argumentName.substring(i), stepsBack);
    }

    protected Object getDefaultValue(final Type type) {
        return Defaults.defaultValue(getClazz(type));
    }

    protected Object convertFieldArgumentValue(final DataFetchingEnvironment env, final Object value, final Type type) {
        return inputObjectDeserializer.deserialize(value, type);
    }

    /**
     * Evaluate the expand= string.
     */
    protected String evaluateExpandArgAsString(
            final DataFetchingEnvironment env,
            final Type methodReturnType,
            final Field field) {

        return String.join(",", evaluateExpandArg(
                methodReturnType,
                env.getFragmentsByName(),
                field));
    }

    protected List<String> evaluateExpandArg(
            final Type methodReturnType,
            final Map<String, FragmentDefinition> fragmentsByName,
            final Field field) {

        if (responseType == null) {
            throw new RuntimeException("responseType is required to evaluate expansions");
        }
        if (allTypes == null) {
            throw new RuntimeException("allTypes is required to evaluate expansions");
        }
        if (field == null) {
            throw new IllegalArgumentException("Query field is required to evaluate expansions");
        }

        final GraphQLExpansionEvaluator expansionEvaluator =
                new GraphQLExpansionEvaluator(responseType, fragmentsByName, allTypes);
        final List<String> rootPaths = extensions != null ? extensions.getExpansionRootPaths(methodReturnType) : null;
        if (rootPaths == null) {
            return expansionEvaluator.evaluateExpansions(field, null);
        }

        final LinkedHashSet<String> list = new LinkedHashSet<>();
        for (final String rootPath : rootPaths) {
            list.addAll(expansionEvaluator.evaluateExpansions(field, rootPath));
        }
        return Lists.newArrayList(list);
    }
}
