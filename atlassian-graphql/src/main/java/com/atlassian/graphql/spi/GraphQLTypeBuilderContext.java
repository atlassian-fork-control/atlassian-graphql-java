package com.atlassian.graphql.spi;

import com.atlassian.graphql.annotations.GraphQLName;
import com.atlassian.graphql.provider.RootProviderGraphQLTypeBuilder;
import com.google.common.collect.Iterables;
import graphql.schema.GraphQLObjectType;
import graphql.schema.GraphQLType;
import graphql.schema.GraphQLTypeReference;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.function.BiFunction;
import java.util.function.Predicate;
import java.util.function.Supplier;

import static java.util.Objects.requireNonNull;

/**
 * The object that's passed to {@link GraphQLTypeBuilder} while building graphql types.
 */
public class GraphQLTypeBuilderContext {
    private GraphQLTypeBuilder typeBuilder;
    private GraphQLExtensions extensions;
    private final Map<String, GraphQLType> types = new LinkedHashMap<>();
    private final Set<String> typesBuilding = new HashSet<>();
    private final List<String> currentFieldPath = new ArrayList<>();
    private final List<Type> currentTypePath = new ArrayList<>();
    private BiFunction<String, Object, GraphQLObjectType> providerGraphQLTypeBuilder;

    public GraphQLTypeBuilderContext() {
    }

    public GraphQLTypeBuilderContext(final GraphQLExtensions extensions) {
        this(null, extensions);
    }

    public GraphQLTypeBuilderContext(final GraphQLTypeBuilder typeBuilder) {
        this(typeBuilder, null);
    }

    public GraphQLTypeBuilderContext(final GraphQLTypeBuilder typeBuilder, final GraphQLExtensions extensions) {
        this.typeBuilder = typeBuilder;
        this.extensions = extensions;
    }

    public GraphQLTypeBuilder getTypeBuilder() {
        return typeBuilder;
    }

    public void setTypeBuilder(final GraphQLTypeBuilder typeBuilder) {
        this.typeBuilder = typeBuilder;
    }

    public GraphQLExtensions getExtensions() {
        return extensions;
    }

    public void setExtensions(final GraphQLExtensions extensions) {
        this.extensions = extensions;
    }

    /**
     * Get the built graphql types.
     */
    public Map<String, GraphQLType> getTypes() {
        return types;
    }

    /**
     * Add a type that has been built, so that it can be referenced later using a {@link GraphQLTypeReference}
     * to avoid recursively re-building the same type.
     * The types will be passed to GraphQLSchema.Builder.build().
     */
    public void addType(final GraphQLType type) {
        requireNonNull(type);
        types.put(type.getName(), type);
    }

    /**
     * Get whether a type has been built, or is in the process of being built.
     */
    public boolean hasTypeOrIsBuilding(final String name) {
        requireNonNull(name);
        return typesBuilding.contains(name) || types.containsKey(name);
    }

    /**
     * Mark a type name as being built, to avoid recursively building the same type.
     */
    public void typeBuildStarted(final String name) {
        requireNonNull(name);
        typesBuilding.add(name);
    }

    /**
     * Called when starting to build a type for a nested field.
     */
    public void enterField(final String fieldName, final Type fieldType) {
        requireNonNull(fieldName);
        currentFieldPath.add(fieldName);
        currentTypePath.add(fieldType);
    }

    /**
     * Called when finished building a type for a nested field.
     */
    public void exitField() {
        currentFieldPath.remove(currentFieldPath.size() - 1);
        currentTypePath.remove(currentTypePath.size() - 1);
    }

    /**
     * Temporarily update the current field type.
     */
    public <T> T updateFieldType(final Type elementType, final Supplier<T> action) {
        requireNonNull(elementType);
        requireNonNull(action);

        if (currentTypePath.isEmpty()) {
            return action.get();
        }

        final int last = currentTypePath.size() - 1;
        final Type originalType = currentTypePath.get(last);
        currentTypePath.set(last, elementType);
        try {
            return action.get();
        } finally {
            currentTypePath.set(last, originalType);
        }
    }

    /**
     * Get the current field path.
     */
    public List<String> getCurrentFieldPath() {
        return new ArrayList<>(currentFieldPath);
    }

    /**
     * Get the current field type path.
     */
    public List<Type> getCurrentTypePath() {
        return new ArrayList<>(currentTypePath);
    }

    /**
     * Get whether the current type at the end of the path matches exactly the given type.
     */
    public boolean isCurrentType(final Type type) {
        return isCurrentType(type::equals);
    }

    /**
     * Get whether the current type at the end of the path matches the given predicate.
     */
    public boolean isCurrentType(Predicate<Type> typePredicate) {
        return Optional.ofNullable(Iterables.getLast(currentTypePath, null))
                .map(typePredicate::test)
                .orElse(false);
    }

    /**
     * Set a provider type builder, for the call to buildProviderGraphQLType().
     * See {@link RootProviderGraphQLTypeBuilder}.
     */
    public void setProviderGraphQLTypeBuilder(final BiFunction<String, Object, GraphQLObjectType> providerGraphQLTypeBuilder) {
        this.providerGraphQLTypeBuilder = providerGraphQLTypeBuilder;
    }

    /**
     * Build a GraphQLObjectType object with query fields for a set of provider object methods.
     *
     * @param queryTypeName The name of the top-level graph-ql type
     * @param provider      The provider object whose methods marked with {@link GraphQLName} will become query fields
     * @return The built graph-ql schema type
     */
    public GraphQLObjectType buildProviderGraphQLType(final String queryTypeName, final Object provider) {
        if (providerGraphQLTypeBuilder == null) {
            throw new IllegalArgumentException("providerGraphQLTypeBuilder is not available");
        }
        return providerGraphQLTypeBuilder.apply(queryTypeName, provider);
    }
}
